/*
 * CS43L22.h
 *
 *	STM32F407VG to DAC CS43L22 pin connection
 *
 *   - RESET:         PD4
 *   - SCL:           PB6
 *   - SDA:           PB9
 *   - AIN1x / LRCLK: PA4  (I2S3_WS / SPI3_SS / SPI1_SS)
 *   - MCLK:          PC7  (I2S3_MCK)
 *   - SCLK:          PC10 (I2S3_CK / SPI3_CK)
 *   - SDIN:          PC12 (I2S3_SD / SPI3_MOSI)
 *
 * This driver is written based on:
 * http://www.mind-dump.net/configuring-the-stm32f4-discovery-for-audio
 *
 */

#ifndef __CS43L22_H
#define __CS43L22_H

#include "stm32f4xx.h"

// CS43L22 connection to MCU -UM1472 pp.29
#define I2S3_WS_PIN 		GPIO_Pin_4   		//pin PA4
#define I2S3_MCLK_PIN 		GPIO_Pin_7   		//pin PC4
#define I2S3_SCLK_PIN 		GPIO_Pin_10  		//pin PC10
#define I2S3_SD_PIN 		GPIO_Pin_12  		//pin PC12
#define RESET_PIN			GPIO_Pin_4  		//pin PD4
#define I2C_SCL_PIN			GPIO_Pin_6  		//pin PB6
#define I2C_SDA_PIN			GPIO_Pin_9  		//pin PB9

//use I2C1 and SPI3 for CS43L22 configuration and communication
#define CS43L22_I2C 	I2C1
#define CS43L22_I2S 	SPI3

//I2C address of CS43L22
#define CS43L22_I2C_ADDRESS 		0x94
#define CS43L22_CORE_I2C_ADDRESS 	0x33

//Memory Address Pointers
#define CS43L22_ID 						0x01
#define CS43L22_POWER_CTL_1 			0x02
#define CS43L22_POWER_CTL_2 			0x04
#define CS43L22_CLOCKING_CTL			0x05
#define CS43L22_INTERFACE_CTL_1			0x06
#define CS43L22_INTERFACE_CTL_2			0x07
#define CS43L22_PASSTHROUGH_A_SELECT	0x08
#define CS43L22_PASSTHROUGH_B_SELECT	0x09
#define CS43L22_ANALOG_ZC_SR			0x0A
#define CS43L22_PASSTHROUGH_GANG_CTRL	0x0C
#define CS43L22_PLAYBACK_CTL_1			0x0D
#define CS43L22_MISC_CTL				0x0E
#define CS43L22_PLAYBACK_CTL_2			0x0F
#define CS43L22_PASSTHROUGH_A_VOL		0x14
#define CS43L22_PASSTHROUGH_B_VOL		0x15
#define CS43L22_PCMA_VOL				0x1A
#define CS43L22_PCMB_VOL				0x1B
#define CS43L22_BEEP_FREQ_ON_TIME		0x1C
#define CS43L22_BEEP_VOL_OFF_TIME		0x1D
#define CS43L22_BEEP_TONE_CFG			0x1E
#define CS43L22_TONE_CTL				0x1F
#define CS43L22_MASTER_A_VOL			0x20
#define CS43L22_MASTER_B_VOL			0x21
#define CS43L22_HEADPHONE_A_VOLUME		0x22
#define CS43L22_HEADPHONE_B_VOLUME		0x23
#define CS43L22_SPEAKER_A_VOLUME		0x24
#define CS43L22_SPEAKER_B_VOLUME		0x25
#define CS43L22_CHANNEL_MIXER_AND_SWAP	0x26
#define CS43L22_LIMIT_CTL_1_THRESHOLDS	0x27
#define CS43L22_LIMIT_CTL_2_RR			0x28
#define CS43L22_LIMITER_ATTACK_RATE		0x29
#define CS43L22_OVERFLOW_CLOCK_STATUS	0x2E
#define CS43L22_BATTERY_COMPENSATION	0x2F
#define CS43L22_VP_BATTERY_LEVEL		0x30
#define CS43L22_SPEAKER_STATUS			0x31
#define CS43L22_CHARGE_PUMP_FREQUENCY	0x34

void CS43L22_Init(void);
void CS43L22_register_write(uint8_t controlBytes[], uint8_t numBytes);
uint8_t CS43L22_register_read(uint8_t mapbyte);
void CS43L22_desired_settings(void);
void CS43L22_required_settings(void);
void Passthrough_Volume_Low(void);
void Passthrough_Volume_High(void);


#endif /*__CS43L22_H */



